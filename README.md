## My adventures building a Chrome extension


A couple weeks ago I created my first Chrome extension, and here I want to document the journey including the steps and missteps I took.

### Reasons for making
I have always been a fan of Chrome extensions, and somehow had never made one myself up until recently, despite having plenty of "what if" moments imagining how websites I frequently use could be made slightly better. In this case which I finally acted upon, the site happened to be the Firebase developer console's hosting tab. This tab displays the DNS records one must add to the registrar from which the domain was purchased. There are two IP Address values one must copy over, and after a while I thought, "what if this copy paste could be automated?". The end result now [lives on the Chrome Webstore](https://chrome.google.com/webstore/detail/firebase-hosting-%3C-%3E-name/hofkmmcbegmaoeknechdleppgdglmnmp).

[Namecheap](https://www.namecheap.com/) is a registrar which provides services on domain name registration. A couple years ago they added two-factor authentication as an option for additional account security. 2FA is great, but it makes logging into your dashboard require slightly more effort. 

Initial research for how to accomplish:
Luckily Google has made plenty of documentation and guides available covering all aspects of extensions, and they can be found [here](https://developer.chrome.com/extensions). There are also countless articles on Medium and other blogs, serving as great complementary reading since seeing the same info described from different perspectives gives a fuller picture.


### Initial Development + dealing with errors:
It started with doing a whole lot of copying and pasting, making sure the manifest.json file had everything it needs to contain. My first real roadblock had to do with figuring out how dynamically add a button to the existing webpage. The particular challenge was that the modal I wanted that button to display inside of did not exist in the DOM on page load. Furthermore, the ID assigned to that modal appeared to be random due to it being different upon page every refresh:

![Screenshot of changing IDs](changing_ids.png?raw=true "changing_ids")

That led me to try a different approach, utilizing the popup window that appears after clicking on the Chrome extension's icon in the upper right-hand corner of the browser as the way of connecting the Firebase console to Namecheap's API.


### Switching from plain JS to React
There came a point where I realized there was too much conditional logic affecting the content of the popup. For example, I needed to take into account whether 
1. the user has the Namecheap API credentials already saved to the extension's local storage.
2. the user is currently on the Firebase console hosting tab's URL
3. the user is has the "Connect domain" modal open
4. a request is being made with the user's Namecheap API credentials
5. the extention successfully retrieved those credentials from the API call
etc.

All of the quickly became messy so switching to React appeared to be a reasonable option. [This guide](https://engineering.musefind.com/how-to-build-a-chrome-extension-with-react-js-e2bae31747fc) gave a great starting point for doing so with `create-react-app`. The end-result [App.js](https://github.com/PatNeedham/firebase-namecheap-chrome-extension/blob/master/src/App.js) file contains the conditionals for driving those five scenarios mentioned above.

When the component first mounts, three asynchronous actions take place, for 1) getting the user's IP address, 2) getting the locally saved data, and getting the URL of the current tab. The values retreived determine what next appears in the popup window.

### Namecheap API
Namecheap offers [an API](https://www.namecheap.com/support/api/intro.aspx) that lets users do most things that they can on the web dashboard. The only two methods I needed to use were [getHosts](https://www.namecheap.com/support/api/methods/domains-dns/get-hosts.aspx) and [setHosts](https://www.namecheap.com/support/api/methods/domains-dns/set-hosts.aspx). These, along with all other API methods, require certain request parameters to exist such as `ApiUser` and `ApiKey`. Responses come back in XML format, which I had never dealt with in JavaScript before. This was not a major roadblock, but led to a healthy dose of trial and error while attempting to access any response errors. Using something like this:
```js
const errors = xmlDoc.getElementsByTagName('Error') || [];
```
was sufficient because that `Error` tag will only appear when there is at least one.

One thing I had to watch out for in using the `setHosts` method was making sure I didn't accidentally delete existing DNS Host records, or at least not accidentally. I say not *accidentally* because as per the API docs:

> All host records that are not included into the API call will be deleted, so add them in addition to new host records.

The only time I can see it being beneficial to delete records is to get rid of the two that exist by default after registering a new domain, one which is of type `CNAME Record` with value `http://parkingpage.namecheap.com.`, and the other being a `URL Redirect Record`. Since you would have to remove those eventually in order to see actual content on the site.

### Publishing to the Chrome Webstore

Compared to the process of developing this application, actually publishing it to the Chrome Webstore was surprisingly easy. The steps are succinctly described [here](https://medium.freecodecamp.org/how-to-publish-your-chrome-extension-dd8400a3d53). A few screenshots along with a one-time $5 registration fee are the main necessities. After filling out other info (like the category this extension belongs to, and whether want to restrict its access from certain countries), and uploading a zip file of the extension, it becomes accessible for anyone to download.

### Next Steps

This extension I made was mostly for educational purposes, and without a doubt has at least a bug or three. However, it helped pave a path toward making other extensions which have less niche use cases, such as [one](https://chrome.google.com/webstore/detail/snippet-copier-shortcut/clmacdbnemcndlnlpkdipclbcpdiabjd#) that adds custom "copy-to-clipboard" buttons with dynamically changing keyboard shortcuts to the page for every code snippet in a readme file on Github.